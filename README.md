# README #

Gracias por confiar en nosotros! Es necesario que lea esto para una optima utilizacion de la pagina

### <h2 style="color:#de4fff"><b>Imagenes</b><h2> ###

En cuanto a las imagenes es necesario que a la hora de subir la pagina al servidor (incluyendo las imagenes), referencie el link de ubicacion de la imagen

A su vez hay una carpeta SVG donde estan subidos los distintos vectores que usamos de detalles, estos tambien deben ser referenciados denuevo.

A la hora de referenciar las imagenes, fijese que en el CSS hay unas clases llamadas hero1,hero2,hero3 que tienen tambien que referenciarse las imagenes



### <h2 style="color:#de4fff"><b>Footer</b><h2> ###

En el footer dejamos listo todo para que unicamente tenga que completar con los datos que no fueron proporcionados

* Ubicacion de referencia
* Email de contacto
* Telefono de referencia


### <h2 style="color:#de4fff"><b>Mail</b><h2> ###

Puntos a tener en cuanta para que funcione el enviar mensaje

* hay un archivo .php ahi hay unas anotaciones para poder customizar el mail. ES NECESARIO QUE LAS LEA
* Configurar el servidor donde se hostee la pagina para que funcione


<br>
<br>
<h2><b> Gracias</b></h2>

Gracias por confiar en nosotros! <br>


<img style="width: 150px; margin-top:10px; margin-bottom:10px;" src="https://firebasestorage.googleapis.com/v0/b/spacetechproyect.appspot.com/o/logo_space.png?alt=media&token=9699b814-854e-467a-9bbc-8b26e70bf44e">

<br>

### space.tech.innovation@gmail.com 