

<!doctype html>
<html lang="es">
  <head>
    
    <?php
    if(isset($_POST["submit"])){
        $nombre = $_POST['name'];
        $mail = $_POST['mail'];
        $dni = $_POST['dni'];
        $postal = $_POST['postal'];
        $pref = $_POST['pref'];
        $mobile = $_POST['mobile'];
        $brand = $_POST['brand'];
        $sum = $_POST['sum'];
        $plan = $_POST['plan'];
        $payment1 = $_POST['payment'];
        $payment2 = $_POST['payment2'];
        $n_card = $_POST['n_card'];
        $v_card = $_POST['v_card'];
        
        $headers  = "MIME-Version: 1.0 \r\n";
        $headers .= "Content-Type: text/plain; charset=UTF-8 \r\n";  
        $headers .= "From: <$mail> \r\n";
        $headers .= "Reply-To: <$mail>";	
        
        $mensaje ="DATOS PERSONALES" ." \r\n";
        $mensaje .= "Nombre: " . $nombre ." \r\n";
        $mensaje .= "DNI: " . $dni . " \r\n";
        $mensaje .= "Email: " . $mail . " \r\n";
        $mensaje .= "Código Postal: " . $postal ." \r\n";
        $mensaje .= "Prefijo: " . $pref . " \r\n";
        $mensaje .= "Teléfono: " . $mobile ." \r\n";

        $mensaje .="DATOS DE LA BICICLETA" ." \r\n";
        $mensaje .= "Marca y modelo: " . $brand . " \r\n";
        $mensaje .= "Suma asegurada: " . $sum ." \r\n";
        $mensaje .="DATOS DEL PAGO" ." \r\n";
        $mensaje .= "Plan a pagar: " . $plan . " \r\n";
        $mensaje .= "Forma de pago: " . $payment1  . " | " . $payment2 . " \r\n";
        $mensaje .= "Numero de tarjeta: " . $n_card .  " \r\n";
        $mensaje .= "Fecha de vencimiento: " . $v_card . " \r\n";
        $mensaje .= "Fue enviado: " . date('d/m/Y', time());
        
        ///COMPLETE CON SU CORREO ELECTRONICO AL CUAL QUIERA RECIBIR EL MENSAJE
        $para = "SUMAIL";
        // PUEDE CAMBIAR EL ASUNTO SI ASI LO DESEA
        $asunto = 'Asegurar Bicicleta';
        mail($para, utf8_decode($asunto), utf8_decode($mensaje), $header);        
    }else if(isset($_POST["contact"])){
        $headers  = "MIME-Version: 1.0 \r\n";
        $headers .= "Content-Type: text/plain; charset=UTF-8 \r\n";  
        $headers .= "From: <$mail> \r\n";
        $headers .= "Reply-To: <$mail>";
        
        $nombre = $_POST['name'];
        $mail = $_POST['mail'];
        $mobile = $_POST['mobile'];
        $value = $_POST['price_bike'];

        $mensaje ="DATOS PERSONALES" ." \r\n";
        $mensaje .= "Nombre: " . $nombre ." \r\n";
        $mensaje .= "Teléfono: " . $mobile . " \r\n";
        $mensaje .= "Email: " . $mail . " \r\n";
        $mensaje .= "Precio de bicicleta: " . $value . " \r\n";
        $mensaje .= "Fue enviado... " . date('d/m/Y', time());

        $para = "SU MAIL";
        $asunto = 'Consulta - Asegurar bicicleta - Monto mayor a 250000';
        
        
        mail($para, utf8_decode($asunto), utf8_decode($mensaje), $header);        
    }


?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="./img/icon.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;900&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="css/index.css" type="text/css">  

    <title>Bike Quote</title>
  </head>
  
    <body class="background_image2">
        <div>
            <nav class="navbar smart-scroll navbar-expand-lg fixed-top navbar-dark transparent_background">
              <a class="navbar-brand" href="index.html">
                <img src="./img/logo_1.png" width="32" height="23" alt="" loading="lazy">
              </a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarNavDropdown">
             <ul class="navbar-nav ml-auto" style="margin-right: 30px;">
               <li class="nav-item active">
                 <a class="nav-link" href="index.html">INICIO</span></a>
               </li>
             </ul>
           </div>
         </nav>
       </div>

    <div class="container">

        <div class="deskContent">
            <h1 class="white_text " style="font-weight: 600;font-size: 80px; margin-top: 20%; text-align: center;">FELICITACIONES!</h1>
        </div>
        <div class="phoneContent">
            <h1 class="white_text " style="font-weight: 600; margin-top: 20%; text-align: center;">FELICITACIONES!</h1>
        </div>
    </div>

    <div class="white_text" style="margin-top:20%;margin-bottom:20%; background-color: #FFAD41;" >
       <div class="container" style="padding-top: 30px; padding-bottom: 20px;">
           <p class="white_text" style=" font-size: 30px; font-weight: 700;">¿CUAL ES EL PROXIMO PASO?</p>
             <p style="margin-top: 30px; font-size: 20px;">Para finalizar el tramite necesitamos que nos envies via mail o whatsapp:
                <ul>
                    <li> 4 fotos de cada lado de la bicicleta</li>
                    <li> Una foto del numero del cuadro que figura debajo de la masa pedalera</li>
                </ul>
            Tras completar estos requerimientos, uno de nuestros asesores se comunicara dentro del transcurso de las <b>24 horas</b>!
        </p>
        <p style="margin-top: 50px;">Gracias por elegirnos!</p>
    

       </div>
    </div>

    <footer class="page-footer font-small" style="background-color: #1D1D1D;">

    <div style="background-color: #FFAD41;">
      <div class="container">
        <div class="row py-4 d-flex align-items-center">
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h6 class="mb-0"></h6>
          </div>
          
        </div>
      </div>
    </div>
    <div class="container text-center text-md-left mt-5">
      <div class="row mt-3">
        <div class="center rowmod col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <img src="/img/logo_white.png" style="width: 300px;">
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <h6 class="text-uppercase font-weight-bold white_text">Contacto</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p class="white_text">
            <i class="fas fa-phone mr-3"><svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-house-door mr-3" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z"/>
              <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
            </svg></i>  CIUDAD, DIRECCION, ARG</p>
          <p class="white_text">
            <i class="fas fa-phone mr-3"><svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-envelope mr-3" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
            </svg></i> info@example.com</p>
          <p class="white_text">
            <i class="fas fa-phone mr-3"><svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-telephone mr-3" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
            </svg></i> + xxxx xxxx-xxxx</p>
          

        </div>

        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <p class="white_text" style="font-weight: 100;"></p>
          <p><i>
          <a href="https://brokerpanamericano.com/index.html"><img style="width: 150px;" src="/img/brokeP.png"></a>
          </i></p>
        </div>
      </div>
    </div>
    <div class="footer-copyright text-center py-3 white_text">Todos los derechos reservados ©2020 
      | <i class="fas mr-1"> <img src="/svg/rellumlogo.svg" style="height: 15px; margin-left: 5px;"> </i> Rellüm Software
    </div>
  </footer>
    </body>
</html>


    


